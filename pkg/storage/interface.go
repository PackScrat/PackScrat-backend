package storage

import "io"

type IStore interface {
	io.Closer
}

type IStoreFactory interface {
	Get() (IStore, error)
}
