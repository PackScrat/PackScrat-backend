package storage

type InMemoryStorageFactory struct {
	MaxSize uint64
	BufferSize uint64
}

func (f *InMemoryStorageFactory) Get() (IStore, error) {
	return &inMemoryStorage{
		maxSize: f.MaxSize,
		buffer: make([]byte, f.BufferSize),
	}, nil
}