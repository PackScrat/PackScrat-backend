package storage

type inMemoryStorage struct {
	maxSize uint64
	buffer []byte
}

func (s *inMemoryStorage) Close() error {
	return nil
}
