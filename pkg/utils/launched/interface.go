package launched

type ILaunched interface {
	Start() error
	Stop() error
}