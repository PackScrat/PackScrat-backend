package core

import (
	"fmt"

	"gitlab.com/PackScrat/PackScrat-backend/pkg/storage"
)

type core struct {

	storage storage.IStore

}



func NewCore(factory storage.IStoreFactory) (ICore, error) {
	storage, err := factory.Get()
	if err != nil {
		return nil, fmt.Errorf("no storage: %w", err)
	}

	return &core{
		storage: storage,
	}, nil
}

func (c *core) Start() error {
	return nil
}

func (c *core) Stop() error {
	return nil
}

func (c *core) Store() storage.IStore {
	return nil
}
