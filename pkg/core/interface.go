package core

import (
	"gitlab.com/PackScrat/PackScrat-backend/pkg/storage"
	"gitlab.com/PackScrat/PackScrat-backend/pkg/utils/launched"
)

type ICore interface {
	launched.ILaunched

	Store() storage.IStore
}
