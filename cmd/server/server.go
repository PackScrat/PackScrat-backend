package main

import (
	"fmt"

	"gitlab.com/PackScrat/PackScrat-backend/pkg/core"
	"gitlab.com/PackScrat/PackScrat-backend/pkg/storage"
)

func main()  {
	fmt.Println("hello server")

	storageFactory := storage.InMemoryStorageFactory{
		MaxSize: 1024*1024*1024, // 1GiB
		BufferSize: 512*1024*1024, // 512MiB
	}

	core, err := core.NewCore(&storageFactory)
	if err != nil {
		fmt.Printf("[ERROR]: %v\n", fmt.Errorf("no core: %w", err))
		return
	}

	if err = core.Start(); err != nil {
		fmt.Printf("[ERROR]: %v\n", fmt.Errorf("core start failed: %w", err))
		return
	}

	if err = core.Stop(); err != nil {
		fmt.Printf("[ERROR]: %v\n", fmt.Errorf("core stop failed: %w", err))
		return
	}

	fmt.Println("bye server")
}